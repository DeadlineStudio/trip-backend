<?php

namespace App\GraphQL\Type;

//use GraphQL\Type\Definition\Type;
//use Rebing\GraphQL\Support\Type as GraphQLType;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\DriverHasCar;
use App\Models\Driver;
use App\Models\Car;

class DriverEarningType extends BaseType
{
    protected $attributes = [
        'name' => 'DriverEarning',
        'description' => 'A driverEarning'
    ];

    public function fields()
    {
        return [
            'driver' => [
                'type' => Type::nonNull(GraphQL::type('Driver'))
            ],
            'ridesDay' => [
                'type' => Type::listOf(GraphQL::type('Ride'))
            ],
            'rides2Week' => [
                'type' => Type::listOf(GraphQL::type('Ride'))
            ],
            'rides1Month' => [
                'type' => Type::listOf(GraphQL::type('Ride'))
            ],
            'rides2Month' => [
                'type' => Type::listOf(GraphQL::type('Ride'))
            ],
            'ridesTotal' => [
                'type' => Type::listOf(GraphQL::type('Ride'))
            ],
            'totalDayTrips' => [
                'type' => Type::nonNull(Type::int())
            ],
            'cumulativeTotal' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    
}
