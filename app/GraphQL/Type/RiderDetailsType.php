<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;
use App\Models\Rating;
use App\Models\Rider;

class RiderDetailsType extends BaseType
{
    protected $attributes = [
        'name' => 'RiderDetails',
        'description' => 'A riderDetails'
    ];

    public function fields()
    {
        return [

            'rider' => [
                'type' => Type::nonNull(GraphQL::type('Rider'))
            ],
            'rating' => [
                'type' => Type::listOf(GraphQL::type('Rating'))
            ],
            'totalRating' => [
                'type' => Type::nonNull(Type::string())
            ],
            'created_at' => [
                'type' => Type::nonNull(Type::string())
            ],
            'updated_at' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at->toDateTimeString();
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at->toDateTimeString();
    }
}
