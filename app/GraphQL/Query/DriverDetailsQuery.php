<?php

namespace App\GraphQL\Query;

use GraphQL;
use JWTAuth;
use DB;
use App\Models\Car;
use App\Models\Driver;
use App\Models\DriverHasCar;
use App\Models\Upload;
use App\Models\RideHasRating;
use App\Models\Ride;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class DriverDetailsQuery extends Query
{
    protected $attributes = [
        'name' => 'driverDetails',
        'description' => 'Driver Details.'
    ];

    public function type()
    {
        return GraphQL::type('DriverHasCarImage');
    }

    public function args()
    {
        return [
            'driverId' => [
                'name' => 'driverId',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $DriverHasCar =[];
        $car =[];
        $date = date("Y-m-d");
        $driverId = $args['driverId'];


        $driver = Driver::where('id',$args['driverId'])->first();
        $DriverHasCarr = DriverHasCar::where('driver_id',$driver->id)->get();


        // $photos = Upload::where('car_id',$DriverHasCar->car_id)->get();
        //$DriverHasCar->car['photos'] = $photos;


        $rides =  DB::select(DB::raw("SELECT * FROM rides WHERE rides.driver_id=$driverId and rides.fecha='$date' and rides.status='Completed'"));
        $total_viajes = count($rides);


        $items =  DB::select(DB::raw("SELECT ratings.* FROM ride_has_ratings JOIN rides ON rides.id = ride_has_ratings.ride_id JOIN ratings ON ratings.id = ride_has_ratings.rating_id WHERE ratings.valuedBy='Rider' and rides.driver_id='$driverId'"));


        $total = 0;
        foreach ($items as $key => $value) {
            $total = $total + $value->total;
        }

        $totalAcumulado = 0;
        foreach ($rides as $key => $value) {
            $totalAcumulado = $totalAcumulado + $value->totalPrice;
        }


        foreach ($DriverHasCarr as $key => $value) {
            $carr = Car::where('id',$value->car_id)->first();
            $photos = Upload::where('car_id',$value->car_id)->get();
            $carr['photos'] = $photos;
            array_push($car, $carr);
        }

        $DriverHasCar['cars']=$car;



        $DriverHasCar['driver'] = $driver;
        $DriverHasCar['rating'] = $items;
        $DriverHasCar['totalDayTrips'] = $total_viajes;
        $DriverHasCar['cumulativeTotal'] = $totalAcumulado;

        if ($total === 0) {
            $DriverHasCar['totalRating'] = $total;
        }else{
            $DriverHasCar['totalRating'] = $total / count($items);

        }
        
        return $DriverHasCar;


    }
}
