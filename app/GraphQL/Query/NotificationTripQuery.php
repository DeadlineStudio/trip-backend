<?php

namespace App\GraphQL\Query;

use GraphQL;
use JWTAuth;
use DB;
use App\Models\Rider;
use App\Models\Ride;
use App\Models\DriverHasCar;
use App\Models\Upload;
use App\Models\RideHasRating;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class NotificationTripQuery extends Query
{
    protected $attributes = [
        'name' => 'notificationTrip',
        'description' => 'notificationTrip Details.'
    ];

    public function type()
    {
        return GraphQL::type('NotificationRide');
    }

    public function args()
    {
        return [
            'rideId' => [
                'name' => 'rideId',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $response=[];
        $rideId = $args['rideId'];
        $ride = Ride::where('id',$args['rideId'])->first();
        //$response['rides'] = $rider;


        $items =  DB::select(DB::raw("SELECT ratings.* FROM ride_has_ratings JOIN rides ON rides.id = ride_has_ratings.ride_id JOIN ratings ON ratings.id = ride_has_ratings.rating_id WHERE ratings.valuedBy='Rider' and rides.rider_id='$ride->rider_id'"));


        $total = 0;
        foreach ($items as $key => $value) {
            $total = $total + $value->total;
        }

        $ride->driver['rating'] = $items;

        if ($total === 0) {
            $ride->driver['totalRating'] = $total;
        }else{
            $ride->driver['totalRating'] = $total / count($items);

        }
        
        return $ride;


    }
}
