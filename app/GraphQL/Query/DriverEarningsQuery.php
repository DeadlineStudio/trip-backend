<?php

namespace App\GraphQL\Query;

use GraphQL;
use JWTAuth;
use DB;
use App\Models\Car;
use App\Models\Driver;
use App\Models\DriverHasCar;
use App\Models\Upload;
use App\Models\RideHasRating;
use App\Models\Ride;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class DriverEarningsQuery extends Query
{
    protected $attributes = [
        'name' => 'driverEarnings',
        'description' => 'Driver Earnings.'
    ];

    public function type()
    {
        return GraphQL::type('DriverEarning');
    }

    public function args()
    {
        return [
            'driverId' => [
                'name' => 'driverId',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $DriverHasCar =[];
        $car =[];
        $date = date("Y-m-d");
        $week2 =date("Y-m-d", strtotime("$date -2 week"));
        $month1 = date("Y-m-d", strtotime("$date -1 month"));
        $month2 = date("Y-m-d", strtotime("$date -2 month"));
        $driverId = $args['driverId'];


        $driver = Driver::where('id',$args['driverId'])->first();
        $DriverHasCarr = DriverHasCar::where('driver_id',$driver->id)->get();



        $ridesDay = Ride::Where('rides.driver_id',$driverId)
                        ->Where('rides.fecha',$date)
                        ->Where('rides.status',"Completed")
                        ->get();

        $ridesTotal = Ride::where('driver_id',$driverId)
                        ->orWhere('status',"Completed")
                        ->get();


        $rides2Week = Ride::where('rides.driver_id',$driverId)
                  ->whereBetween('rides.fecha', [$week2, $date])
                  ->orderBy('rides.fecha','ASC')
                  ->get();
                  

        $rides1Month = Ride::where('rides.driver_id',$driverId)
                  ->whereBetween('rides.fecha', [$month1, $date])
                  ->orderBy('rides.fecha','ASC')
                  ->get();


        $rides2Month = Ride::where('rides.driver_id',$driverId)
                  ->whereBetween('rides.fecha', [$month2, $date])
                  ->orderBy('rides.fecha','ASC')
                  ->get();
                


        $total_viajes = count($ridesDay);



        $totalAcumulado = 0;
        foreach ($ridesDay as $key => $value) {
            $totalAcumulado = $totalAcumulado + $value->totalPrice;
        }


        $DriverHasCar['driver'] = $driver;
        $DriverHasCar['ridesDay'] = $ridesDay;
        $DriverHasCar['rides2Week'] = $rides2Week;
        $DriverHasCar['rides1Month'] = $rides1Month;
        $DriverHasCar['rides2Month'] = $rides2Month;
        $DriverHasCar['ridesTotal'] = $ridesTotal;
        $DriverHasCar['totalDayTrips'] = $total_viajes;
        $DriverHasCar['cumulativeTotal'] = $totalAcumulado;


        
        return $DriverHasCar;


    }
}
