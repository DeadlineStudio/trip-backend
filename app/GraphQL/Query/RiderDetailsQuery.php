<?php

namespace App\GraphQL\Query;

use GraphQL;
use JWTAuth;
use DB;
use App\Models\Rider;
use App\Models\DriverHasCar;
use App\Models\Upload;
use App\Models\RideHasRating;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;

class RiderDetailsQuery extends Query
{
    protected $attributes = [
        'name' => 'driverDetails',
        'description' => 'Driver Details.'
    ];

    public function type()
    {
        return GraphQL::type('RiderDetails');
    }

    public function args()
    {
        return [
            'riderId' => [
                'name' => 'riderId',
                'type' => Type::nonNull(Type::int())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        /*try {
            $this->auth = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            $this->auth = null;
            throw new \Exception("Unauthorized", 403);
        }*/

        $response=[];
        $riderId = $args['riderId'];
        $rider = Rider::where('id',$args['riderId'])->first();
        $response['rider'] = $rider;


        $items =  DB::select(DB::raw("SELECT ratings.* FROM ride_has_ratings JOIN rides ON rides.id = ride_has_ratings.ride_id JOIN ratings ON ratings.id = ride_has_ratings.rating_id WHERE ratings.valuedBy='Driver' and rides.rider_id='$riderId'"));


        $total = 0;
        foreach ($items as $key => $value) {
            $total = $total + $value->total;
        }

        $response['rating'] = $items;

        if ($total === 0) {
            $response['totalRating'] = $total;
        }else{
            $response['totalRating'] = $total / count($items);

        }
        
        return $response;


    }
}
